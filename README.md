#### Criando projeto
```
ng new gerenciador-de-tarefas
```

#### Instalando o Bootstrap
```
npm install --save bootstrap@3
```

#### Criando módulo de tarefas
```
ng g module tarefas
```

#### Criando um serviço de gerenciamento de tarefas
```
ng g service tarefas/shared/tarefa
```

#### Criando o componente de listagem de tarefas (sempre apontar para pasta onde se encontra o modulo e dar um nome)
```
ng g component tarefas/listar-tarefa
```


#### Criando componente de cadastro de tarefas
```
ng g component tarefas/cadastrar-tarefa
```


#### Criando componente de edição de tarefas
```
ng g component tarefas/editar-tarefa
```

#### Criando uma diretiva de tarefa concluída
```
ng g directive tarefas/shared/tarefa-concluida
```

##### Componente gera uma tag HTML, Diretiva gera um atributo HTML