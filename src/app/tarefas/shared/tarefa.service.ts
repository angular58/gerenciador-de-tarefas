import { Injectable } from '@angular/core';
import {TarefaModel} from "./tarefa.model";

@Injectable()
export class TarefaService {

  static readonly LOCAL_STORAGE = 'tarefas';

  constructor() { }

  listarTodos(): TarefaModel[] {
    const tarefas = localStorage[TarefaService.LOCAL_STORAGE];
    return tarefas ? JSON.parse(tarefas) : [];
  }

  cadastrar(tarefa: TarefaModel): void {
    const tarefas = this.listarTodos();
    tarefa.id = new Date().getTime();
    tarefas.push(tarefa);
    localStorage['tarefas'] = JSON.stringify(tarefas);
  }

  buscarPorId(id: number): TarefaModel {
    const tarefas: TarefaModel[] = this.listarTodos();
    return tarefas.find(tarefa => tarefa.id == id);
  }

  atualizar(tarefa: TarefaModel): void {
    const tarefas: TarefaModel[] = this.listarTodos();
    tarefas.forEach((obj, index, objs) => {
      if (tarefa.id === obj.id) {
        objs[index] = tarefa;
      }
    });
    localStorage[TarefaService.LOCAL_STORAGE] = JSON.stringify(tarefas);
  }

  remover(id: number): void {
    let tarefas: TarefaModel[] = this.listarTodos();
    tarefas = tarefas.filter(tar => tar.id !== id);
    localStorage[TarefaService.LOCAL_STORAGE] = JSON.stringify(tarefas);
  }

  alterarStatus(id: number): void {
    const tarefas: TarefaModel[] = this.listarTodos();
    tarefas.forEach((tarefa, index, tarefas) => {
      if (tarefa.id === id) {
        tarefas[index].concluida = !tarefa.concluida;
      }
    });
    localStorage[TarefaService.LOCAL_STORAGE] = JSON.stringify(tarefas);
  }

}
