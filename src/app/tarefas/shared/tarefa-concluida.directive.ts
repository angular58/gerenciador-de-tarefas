import {Directive, ElementRef, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[tarefaConcluida]'
})
export class TarefaConcluidaDirective implements OnInit {

  // @Input é o valor passado na atribuição da propriedade da diretiva
  // Ex.: <input appTarefaConcluida="true">
  @Input() tarefaConcluida: boolean;

  constructor(private el: ElementRef) { }

  ngOnInit(): void {
    if (this.tarefaConcluida) {
      this.el.nativeElement.style.textDecoration = 'line-through';
    }
  }

}
