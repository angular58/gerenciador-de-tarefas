import {Component, OnInit, ViewChild} from '@angular/core';
import {TarefaService} from "../shared/tarefa.service";
import {Router} from "@angular/router";
import {TarefaModel} from "../shared/tarefa.model";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-cadastrar-tarefa',
  templateUrl: './cadastrar-tarefa.component.html',
  styleUrls: ['./cadastrar-tarefa.component.css']
})
export class CadastrarTarefaComponent implements OnInit {

  // traz referência do componente html para o componente.ts
  @ViewChild('formTarefa', {static: true}) formTarefa: NgForm;
  tarefa: TarefaModel;

  constructor(
    private tarefaService: TarefaService,
    private router: Router) { }

  ngOnInit(): void {
    this.tarefa = new TarefaModel();
  }

  cadastrar(): void {
    if (this.formTarefa.form.valid) {
      this.tarefaService.cadastrar(this.tarefa);
      this.router.navigate(['/tarefas']);
    }
  }

}
