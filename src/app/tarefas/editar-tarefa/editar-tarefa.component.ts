import {Component, OnInit, ViewChild} from '@angular/core';
import {TarefaService} from "../shared/tarefa.service";
import {ActivatedRoute, Router} from "@angular/router";
import {TarefaModel} from "../shared/tarefa.model";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-editar-tarefa',
  templateUrl: './editar-tarefa.component.html',
  styleUrls: ['./editar-tarefa.component.css']
})
export class EditarTarefaComponent implements OnInit {

  @ViewChild('formTarefa', {static: true}) formTarefa: NgForm;
  tarefa: TarefaModel;

  constructor(private tarefaService: TarefaService,
              private activatedRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    let id: number = this.activatedRoute.snapshot.params['id'];
    this.tarefa = this.tarefaService.buscarPorId(id);
  }

  atualizar(): void {
    if (this.formTarefa.form.valid) {
      this.tarefaService.atualizar(this.tarefa);
      this.router.navigate(['/tarefas']);
    }
  }

}
