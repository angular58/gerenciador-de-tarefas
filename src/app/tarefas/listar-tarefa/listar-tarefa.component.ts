import { Component, OnInit } from '@angular/core';
import {TarefaService} from "../shared/tarefa.service";
import {TarefaModel} from "../shared/tarefa.model";

@Component({
  selector: 'app-listar-tarefa',
  templateUrl: './listar-tarefa.component.html',
  styleUrls: ['./listar-tarefa.component.css']
})
export class ListarTarefaComponent implements OnInit {

  tarefas: TarefaModel[];

  constructor(private tarefaService: TarefaService) { }

  ngOnInit(): void {
    this.tarefas = this.listarTodos();
  }

  listarTodos(): TarefaModel[] {
    return this.tarefaService.listarTodos();
  }

  // $event é o evento padrão do navegador
  remover($event: any, tarefa: TarefaModel): void {
    $event.preventDefault();
    if (confirm(`Deseja remover a tarefa ${tarefa.nome}?`)) {
      this.tarefaService.remover(tarefa.id);
      this.tarefas = this.tarefaService.listarTodos();
    }
  }

  alterarStatus(tarefa: TarefaModel): void {
    if (confirm(`Deseja alterar o status da tarefa ${tarefa.nome}?`)) {
      this.tarefaService.alterarStatus(tarefa.id);
      this.tarefas = this.tarefaService.listarTodos();
    }
  }

}
